<?php

/*
Template Name: Аналитика
*/
get_header();

?>

<div id="content">
    <div class="block-wrap">
        <h1 class="page-title"><?= the_title(); ?></h1>
        <div class="field-content">
            <div class="anal"> <!-- ;)) -->
                <?php $topslider = new WP_Query(array('post_type' => 'videoreview', 'post_formats' => 'video', 'posts_per_page' => 3, 'order' => 'DESC')); ?>
                <?php if ($topslider->have_posts()): ?><?php while ($topslider->have_posts()): $topslider->the_post(); ?>
                    <div class="row row-1">
                        <div class="field-title">
                            <?= the_title(); ?>
                        </div>
                        <div class="field-body">
                            <?= the_content(); ?>
                        </div>
                    </div>
                <?php endwhile; ?><?php else: ?>
                    <p>Видео новостей сейчас нет</p>
                <?php endif; ?>
            </div> <!-- end anal -->
            <div class="entrance">
                <div class="left">Для более точной информации вступайте клуб трейдеров!</div>
                <div class="right"><a href="#">Вступить в клуб трейдеров</a></div>
            </div>
        </div>
        <div class="sidebar-right">
            <div class="an-news">
                <?php $topslider = new WP_Query(array('post_type' => 'right_block_articles', 'posts_per_page' => 6, 'order' => 'DESC')); ?>
                <?php
                if ($topslider->have_posts()): ?><?php while ($topslider->have_posts()): $topslider->the_post();

                    $date = get_field('_date_news');
                    ?>
                    <div class="row">
                        <div class="block1">
                            <div class="field-img"><img width="68" src="<?=get_the_post_thumbnail_url(null, array(68, 68)); ?>" alt="<?php the_title(); ?>"></div>
                            <div class="field-date"><?=$date; ?></div>
                        </div>
                        <div class="field-title"><a href="<?=the_permalink();?>"><?php the_title(); ?></a></div>
                        <div class="fiedl-body">
                            <?php the_excerpt(); ?>
                        </div>
                    </div>
                <?php endwhile; ?><?php else: ?>
                    <p>Видео новостей сейчас нет</p>
                <?php endif; ?>
            </div>
        </div> <!-- end right-sidebar -->
    </div>
</div> <!-- end content -->


<div id="footer">
    <div class="block-wrap">
        <div class="block1">
            <div class="logo2">
                <img src="images/logo.png" alt="">
            </div>
            <div class="text-logo">
                академия <br>
                профессионального <br>
                трейдинга
            </div>
        </div>
        <div class="block2">
            <div class="menu">
                <ul>
                    <li><a href="">главная </a></li>
                    <li><a href="">аналитика </a></li>
                    <li><a href="">Клуб трейдеров </a></li>
                    <li><a href="">обучение </a></li>
                    <li><a href="">инвесторам</a></li>
                    <li><a href="">экономический календарь</a></li>
                    <li><a href="">о нас </a></li>
                    <li><a href="">контакты</a></li>
                </ul>
            </div>
        </div>
        <div class="block3">
            <div class="contacts">
                <div class="telephone">
                    8 800 345 55 88
                </div>
                <a href="#form2" class="btn btn1">Обратный звонок</a>
            </div>
        </div>
        <div class="block4">
            Rusmars является одним из крупнейших Форекс-брокеров не только в России, но и в мире. Мы предлагаем нашим
            клиентам большой спектр качественных услуг и современных сервисов для интернет-трейдинга на валютном рынке:
            самостоятельную торговлю на Forex,
            инвестиционные решения на Forex,
        </div>
        <div class="block5">
            <div class="left">
                © 2016 Rusmars «Академия Профессионального Трейдинга»
            </div>
            <div class="right">
                Сайт разработан в <a href="http://adtherapy.ru/">Рекламотерапия</a>
            </div>
        </div>
    </div>
</div> <!-- end footer -->
</div>    <!-- end-wrap -->

<script>

    $(document).ready(function () {
        $('.flexslider').flexslider({
            animation: "fade",
            animationLoop: true,
            itemWidth: 210,
            itemMargin: 0,
            pauseOnHover: true,
            controlNav: true,
            directionNav: false,
            minItems: 1,
            maxItems: 1
        });
    });


    $(".ajaxForm").submit(function (event) {
        event.preventDefault();
        var flag = 1;
        $(this).find('input[name=phone]').each(function () {
            if (!$(this).val().length) {
                swal({
                    title: "Неправильно заполнены поля:",
                    text: "Поле «Телефон» обязательно для заполнения.",
                    type: "error",
                    confirmButtonText: "Хорошо",
                    allowOutsideClick: true
                });
                flag = 0;
            } else {
                $(this).css('border', 'none');
            }
        });
        if (flag == 1) {
            var form = $(this);
            var text = $(this).find("button").text();
            console.log(text);
            var message = form.serialize() + window.location.search.replace('?', '&');
            $.ajax({
                type: "POST",
                url: $(this).attr('action'),
                data: message,
                success: function (html) {
                    swal({
                        title: "Спасибо",
                        text: "В ближайшее время мы свяжемся с вами!",
                        type: "success",
                        confirmButtonText: "Хорошо",
                        allowOutsideClick: true
                    });
                }
            });
        }
    });
</script>

<script type="text/javascript" src="js/script.js"></script>
<script>


    jQuery(window).load(function () {
        jQuery('.gallery-wrap').masonry({columnWidth: 0, itemSelector: '.field-img'});
    });
    jQuery(window).load(function () {
        jQuery('.reviews-wrap').masonry({columnWidth: 0, itemSelector: '.field-img'});
    });

    jQuery(window).load(function () {
        jQuery('.gallery.gal2').css({'display': 'none'});
    });

</script>
</body>
</html>
<?php

/*
Template Name: Registration
*/
get_header();

?>

<div id="content">
    <div class="block-wrap">
        <h1 class="page-title">Регистрация в личном кабинете</h1>

    </div>
</div> <!-- end content -->
<div class="triptych">
    <div class="block-wrap">
        <div class="block-registration">
            <?php custom_registration_function(); ?>
        </div>
    </div>
</div> <!-- end triptych -->
<?php get_footer();?>
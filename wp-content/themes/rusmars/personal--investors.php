<?php

/*
Template Name: ЛК Инвесторам
*/
session_start();

if (!$_SESSION["is_auth"]) {
    header("Location: /");
}
get_header();

?>

		<div id="content">
			<div class="block-wrap">
				<div class="inv-club">
					<div class="period">
						<div class="block1">Сумма ивестиций</div>
						<div class="block2">Процент по вкладу</div>
						<div class="block3">Дата вклада</div>
					</div>
					<div class="conditions">

						<div class="row row-1">
							<div class="field-price">
								1260 млн. руб
							</div>
							<div class="field-percentage">
								12%
							</div>
							<div class="field-date">
								12.03.2015 
							</div>
						</div>
						<div class="row row-2">
							<div class="field-price">
								1260 млн. руб
							</div>
							<div class="field-percentage">
								12%
							</div>
							<div class="field-date">
								12.03.2015 
							</div>
						</div>
						<div class="row row-3">
							<div class="field-price">
								1260 млн. руб
							</div>
							<div class="field-percentage">
								12%
							</div>
							<div class="field-date">
								12.03.2015 
							</div>
						</div>
						<div class="row row-4">
							<div class="field-price">
								1260 млн. руб
							</div>
							<div class="field-percentage">
								12%
							</div>
							<div class="field-date">
								12.03.2015 
							</div>
						</div>

					</div> <!-- end conditions -->
				</div> <!-- end inv-club -->
			</div>
		</div> <!-- end content -->
		<div class="triptych">
			<div class="block-wrap">
				<h2>Инвесторские программы</h2>
				<div class="program">
					<div class="row row-1">
						<div class="field-content">
							<div class="field-title">
								Стандарт
							</div>
							<div class="field-body">
								от <b>100 000 руб</b> до <b>1 000 000 руб</b> ставка <b>3% в месяц</b>
							</div>
							<form action="" name="standart">
								<div class="f-title">
									Онлайн расчет
								</div>
								<div class="summ">
									<div class="tit">
										Сумма
									</div>
									<div class="field-form-1">
										<div class="label">
											от:
										</div>
										<input type="text" name="s_sum_from" value="100000">
									</div>
									<div class="field-form-2">
										<div class="label">
											до:
										</div>
										<input type="text" name="s_sum_to" value="1000000">
									</div>
								</div>
								<div class="time">
									<div class="tit">
										Срок
									</div>
									<div class="field-form-1">
										<div class="label">
											от:
										</div>
										<input type="text" name="s_date_from" value="3">
									</div>
									<div class="field-form-2">
										<div class="label">
											до:
										</div>
										<input type="text" name="s_date_to" value="24">
									</div>
								</div>
							</form>
							<div class="total">
								<span class="block1">
									Итого:
								</span>
								<span class="block2" id="res_standart">200 000 руб.</span>
								<div class="kap">С капитализацией</div>
                                <div>
                                    <input type="button" value="Инвестировать" class="my_btn">
                                </div>
							</div>
						</div>
					</div>

					<div class="row row-2">
						<div class="field-content">
							<div class="field-title">
								Инвестор
							</div>
							<div class="field-body">
								от 1 000 000 руб до 5 000 000 руб ставка 4% в месяц
							</div>
							<form action="" name="investor">
                                <div class="f-title">
                                    Онлайн расчет
                                </div>
                                <div class="summ">
                                    <div class="tit">
                                        Сумма
                                    </div>
                                    <div class="field-form-1">
                                        <div class="label">
                                            от:
                                        </div>
                                        <input type="text" name="i_sum_from" value="100000">
                                    </div>
                                    <div class="field-form-2">
                                        <div class="label">
                                            до:
                                        </div>
                                        <input type="text" name="i_sum_to" value="1000000">
                                    </div>
                                </div>
                                <div class="time">
                                    <div class="tit">
                                        Срок
                                    </div>
                                    <div class="field-form-1">
                                        <div class="label">
                                            от:
                                        </div>
                                        <input type="text" name="i_date_from" value="3">
                                    </div>
                                    <div class="field-form-2">
                                        <div class="label">
                                            до:
                                        </div>
                                        <input type="text" name="i_date_to" value="24">
                                    </div>
                                </div>
							</form>
							<div class="total">
								<span class="block1">
									Итого:
								</span>
								<span class="block2" id="res_investor">200 000 руб.</span>
								<div class="kap">С капитализацией</div>
                                <div>
                                    <input type="button" value="Инвестировать" class="my_btn">
                                </div>
							</div>
						</div>
					</div>

					<div class="row row-3">
						<div class="field-content">
							<div class="field-title">
								Бизнес
							</div>
							<div class="field-body">
								от 5 000 000 руб ставка 5% в месяц
							</div>
							<form action="" name="business">
                                <div class="f-title">
                                    Онлайн расчет
                                </div>
                                <div class="summ">
                                    <div class="tit">
                                        Сумма
                                    </div>
                                    <div class="field-form-1">
                                        <div class="label">
                                            от:
                                        </div>
                                        <input type="text" name="b_sum_from" value="100000">
                                    </div>
                                    <div class="field-form-2">
                                        <div class="label">
                                            до:
                                        </div>
                                        <input type="text" name="b_sum_to" value="1000000">
                                    </div>
                                </div>
                                <div class="time">
                                    <div class="tit">
                                        Срок
                                    </div>
                                    <div class="field-form-1">
                                        <div class="label">
                                            от:
                                        </div>
                                        <input type="text" name="b_date_from" value="3">
                                    </div>
                                    <div class="field-form-2">
                                        <div class="label">
                                            до:
                                        </div>
                                        <input type="text" name="b_date_to" value="24">
                                    </div>
                                </div>
							</form>
							<div class="total">
								<span class="block1">
									Итого:
								</span>
								<span class="block2" id="res_business">200 000 руб.</span>
								<div class="kap">С капитализацией</div>
                                <div>
                                    <input type="button" value="Инвестировать" class="my_btn">
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> <!-- end triptych -->

<?php get_footer(); ?>
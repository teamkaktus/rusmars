<?php get_header(); ?>
    <div class="block-wrap" ">
        <h3 class="" style="text-align: center; padding-top: 10px;"><?php the_title(); ?></h3>
    </div>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
    <div class="" >
        <div class=""><?php the_post_thumbnail('spec_thumb');?></div>
        <div class="" style="text-align: left;"><?php the_content(); ?></div>
    </div>
<?php endwhile; ?>
<?php else: ?>
<?php endif; ?>

<?php get_footer(); ?>
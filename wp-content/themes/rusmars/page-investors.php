<?php
/*
Template Name: Инвесторам
*/
get_header(); ?>

    <div id="content">
        <div class="block-wrap">
            <h1 class="page-title"><?= the_title(); ?></h1>
            <?php if (have_posts()) : while (have_posts()) : the_post();
            endwhile;
            else:
                _e('Страница пустая');
            endif; ?>
            <div class="investor-block-1 banner">
                <?=the_post_thumbnail(); ?>
            </div>
            <div class="investor-block-2">
                <?=the_content(); ?>
            </div>
        </div>
    </div> <!-- end content -->
    <div class="triptych">
        <div class="block-wrap">

        </div>
    </div> <!-- end triptych -->

<?php get_footer(); ?>
<?php get_header(); ?>
        <?php if(have_posts()): while(have_posts()): the_post(); ?>
                <div class="item-img"><?php the_post_thumbnail();?></div>
                <h4 class="item-h4"><?php the_title(); ?></h4>
                <div class="item-hr"></div>
                <div class="item-p"><p><?php the_excerpt(); ?></p></div>
                <div class="item-a"><a href="<?php the_permalink(); ?>">read more</a></div>
        <?php endwhile; ?>
        <?php else: ?>
            <h1>Nothing is found!</h1>
        <?php endif; ?>
<?php get_footer(); ?>



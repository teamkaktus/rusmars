/*video*/
"use strict";
$(function() {
    $(".youtube").each(function() {
        // Based on the YouTube ID, we can easily find the thumbnail image
        $(this).css({});

        // Overlay the Play icon to make it look like a video player
        $(this).append($('<div/>', {'class': 'play'}));

        $(document).delegate('#'+this.id, 'click', function() {
            // Create an iFrame with autoplay set to true
            var iframe_url = "https://www.youtube.com/embed/" + this.id + "?autoplay=1&autohide=1";
            if ($(this).data('params')) iframe_url+='&'+$(this).data('params');

            // The height and width of the iFrame should be the same as parent
            var iframe = $('<iframe/>', {'frameborder': '0','allowfullscreen': 'allowfullscreen', 'src': iframe_url, 'width': $(this).width(), 'height': $(this).height() })

            // Replace the YouTube thumbnail with YouTube HTML5 Player
            $(this).replaceWith(iframe);
        });
    });
 });
/*end video*/




$("a.diplom").colorbox({
    });

$("a[rel^='group'].images").colorbox({    
    });
$("a[rel='group-diplom']").colorbox({
    width: ('70%'),
    });
$("a.btn").colorbox({
        inline:true
    });

$("a.btn2").colorbox({
        width: ('80%'),
        inline:true
    });





 $(document).ready(function(){
        $('#more').on("click",function(){
                $('.gallery.gal1').css({'height': '100%'});
                $(this).fadeOut(100);
        });
    });
 $(document).ready(function(){
        $('#more3').on("click",function(){
                $('.gallery.gal2').css({'height': '100%'});
                $(this).fadeOut(100);
        });
    });
  $(document).ready(function(){
        $('#more2').on("click",function(){
                $('.reviews').css({'height': '100%'});
                $(this).fadeOut(100);
        });
    });


$(document).ready(function(){
  $('.mob-btn1').click(function(){
  $('#form1').css({'transform': 'translate(0)'});
  })
});
$(document).ready(function(){
  $('#form1 .close').click(function(){
  $('#form1').css({'transform': 'translate(-100%)'});
  })
});







$(document).ready(function(){
  $('.menu-open').click(function(){
  $('.menu-link').css({'transform': 'translate(0)'});
  })
});
$(document).ready(function(){
  $('.menu-link .close').click(function(){
  $('.menu-link').css({'transform': 'translate(-100%)'});
  })
});








// $(function(){
//   $(document).click(function(event) {
//     if ($(event.target).closest("#block-block-15,#block-multiblock-3").length) return;
//     $("#block-multiblock-3").css({'transform': 'translate(-300px)'});
//     event.stopPropagation();
//   });
// });
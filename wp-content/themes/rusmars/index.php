<?php get_header(); ?>
		<div id="content">
			<div class="block-wrap">
				<div class="region1">
					<div id="video">
						<div id="a5yaBSxk1gk" class="youtube various fancybox.iframe" style="width: 304px; height: 360px;" href="https://www.youtube.com/embed/a5yaBSxk1gk?autoplay=1">
							<div class="play"></div>
						</div>
					</div>
					<div class="flexslider">
						  <ul class="slides">
                              <?php $topslider = new WP_Query(array('post_type' => 'topslider' )); ?>
                              <?php if($topslider->have_posts()): ?><?php while($topslider->have_posts()): $topslider->the_post();?>
						    <li>
						      <div class="block">
						      	<div class="title"><?php the_title(); ?></div>
						      	<div class="date"><?php the_excerpt(); ?></div>
						      	<div class="body">
                                    <?php the_content(); ?>
						      	</div>
						      	<div class="more">
						      		<a href="<?php the_permalink(); ?>">Подробнее...</a>
						      	</div>
						      </div>
						    </li>
                              <?php endwhile; ?><?php else: ?>
                                  <p>Here is supposed to be a topslider item</p>
                              <?php endif; ?>
						    <!--<li>
						      <div class="block">
						      	<div class="title">Инвесторам1</div>
						      	<div class="date">До 1 августа 2016г. </div>
						      	<div class="body">
						      		инвестируя от 100 000 руб получаешь гарантированный доход 5% в месяц от суммы инвестиции
						      	</div>
						      	<div class="more">
						      		<a href="">Подробнее...</a>
						      	</div>
						      </div>
						    </li>
						    <li>
						      <div class="block">
						      	<div class="title">Инвесторам2</div>
						      	<div class="date">До 1 августа 2016г. </div>
						      	<div class="body">
						      		инвестируя от 100 000 руб получаешь гарантированный доход 5% в месяц от суммы инвестиции
						      	</div>
						      	<div class="more">
						      		<a href="">Подробнее...</a>
						      	</div>
						      </div>
						    </li>
						    <li>
						      <div class="block">
						      	<div class="title">Инвесторам3</div>
						      	<div class="date">До 1 августа 2016г. </div>
						      	<div class="body">
						      		инвестируя от 100 000 руб получаешь гарантированный доход 5% в месяц от суммы инвестиции
						      	</div>
						      	<div class="more">
						      		<a href="">Подробнее...</a>
						      	</div>
						      </div>
						    </li>-->
						  </ul>
					</div>
				</div> <!-- end region1 -->
				<div class="region2">
					<div class="prem">
						<div class="block">
							<div class="row">
								<div class="field-img"><img src="<?php bloginfo('template_url'); ?>/images/img5.png" alt=""></div>
								<div class="field-body">Многолетний <br> опыт реальной<br> торговли на<br> Форекс</div>
							</div>
							<div class="row">
								<div class="field-img"><img src="<?php bloginfo('template_url'); ?>/images/img6.png" alt=""></div>
								<div class="field-body">Индивидуальное<br> обучение<br> онлайн</div>
							</div>
							<div class="row">
								<div class="field-img"><img src="<?php bloginfo('template_url'); ?>/images/img7.png" alt=""></div>
								<div class="field-body">Сопровождение<br> трейдера после<br> обучения (Клуб<br> трейдеров)</div>
							</div>
							<div class="row">
								<div class="field-img"><img src="<?php bloginfo('template_url'); ?>/images/img8.png" alt=""></div>
								<div class="field-body">Торговая<br> стратегия,<br> дающая реальный<br> результат.</div>
							</div>
							<div class="row">
								<div class="field-img"><img src="<?php bloginfo('template_url'); ?>/images/img9.png" alt=""></div>
								<div class="field-body">Работа в команде<br> (клуб трейдеров)</div>
							</div>
							<div class="row">
								<div class="field-img"><img src="<?php bloginfo('template_url'); ?>/images/img10.png" alt=""></div>
								<div class="field-body">Стабильный<br> доход <br>инвесторам.</div>
							</div>
						</div>
					</div>
					<div id="reg">
						<a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/registration/">
							<div class="block">
								<div class="title">
									Регистрация  <br>
									в личном кабинете
								</div>
								<div class="btn-reg">
									Зарегистрироваться
								</div>
							</div>
						</a>
					</div>
				</div> <!-- end region2 -->
				<div class="region3">
					<div class="news">
                        <iframe
                            src="http://ec.ru.forexprostools.com?columns=exc_flags,exc_currency,exc_importance,exc_actual,exc_forecast,exc_previous&category=_employment,_economicActivity,_inflation,_credit,_centralBanks,_confidenceIndex,_balance,_Bonds&importance=1,2,3&features=datepicker,timezone,timeselector,filters&countries=25,54,29,145,34,70,174,163,32,4,93,138,178,17,39,51,24,72,59,84,75,23,14,48,92,33,106,26,10,6,170,57,107,37,122,15,94,97,68,96,103,188,111,42,109,105,7,172,20,21,43,60,143,87,193,44,125,53,38,56,80,100,52,36,90,112,5,41,46,85,202,63,123,61,45,71,22,113,55,27,12,9,162,121,89,110,11,35&calType=day&timeZone=18&lang=7"
                            width="1000" height="600" frameborder="0" allowtransparency="true" marginwidth="0"
                            marginheight="0"></iframe>
                        <div class="poweredBy" style="font-family: Arial, Helvetica, sans-serif;"><span
                                style="font-size: 11px;color: #333333;text-decoration: none;">Экономический онлайн-календарь от <a
                                    href="http://ru.investing.com/" rel="nofollow" target="_blank"
                                    style="font-size: 11px;color: #06529D; font-weight: bold;" class="underline_link">Investing.com
                                    Россия</a>, ведущего финансового портала</span></div>
						<a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/calendar/" class="btn-all-news">Посмотреть весь календарь</a>
					</div>
				</div> <!-- end region3 -->
			</div>
		</div> <!-- end content -->
		<div class="triptych">
			<div class="block-wrap">
				<div class="region4">
					<?php if(have_posts()): while(have_posts()): the_post(); ?>
					<div class="text1">
						<div class="field-title"><?php the_title(); ?></div>
						<div class=""><?php the_post_thumbnail();?></div>
						<div class="field-body"><?php the_excerpt(); ?> </div>
						<div class=""><a href="<?php the_permalink(); ?>">read more</a></div>
						<!--<div class="field-title">Forex с Rusmars</div>
						<div class="field-body">Rusmars является одним из крупнейших Форекс-брокеров не только
в России, но и в мире. Мы предлагаем нашим клиентам большой спектр качественных услуг и современных сервисов для интернет-трейдинга на валютном рынке: самостоятельную торговлю на Forex, инвестиционные решения на Forex, обучение торговле на Forex и многое другое. Более 1 000 000 клиентов со всего мира,
в том числе из России и стран СНГ, уже выбрали RusmarsInvest в качестве надежного поставщика услуг на рынке Форекс. За свою 17-летнюю историю компания заслужила многочисленные награды
и положительные отзывы от довольных клиентов. </div>
					</div>
					<div class="text2">
						<div class="field-title">Что такое Форекс (FOReign EXchange)?</div>
						<div class="field-body">Рынок Forex (FOReign EXchange) появился в конце 70-х годов, после того как многие страны решили не привязывать цену своей национальной валюты к цене доллара или золота.

Это сформировало международный рынок, на котором валюты могли обмениваться по свободным курсам. На сегодняшний день Форекс — это самый крупный финансовый рынок во всем мире. Неважно, какой город выбран вами для жизни или путешествия — Москва или любой другой город мира, — для доступа ко всем инструментам и возможностям Forex вам необходимы Интернет, счет у Форекс-брокера и торговый терминал для работы на Форекс.</div>
					</div>-->
				</div> <!-- end region4 -->
					<?php endwhile; ?>
					<?php else: ?>
					<?php endif; ?>
			</div>
		</div> <!-- end triptych -->
<?php get_footer(); ?>
		
<?php
//Add css and js files
function load_style_script(){
		wp_enqueue_style('style', get_template_directory_uri() . '/css/style.css');
		wp_enqueue_style('reset', get_template_directory_uri() . '/css/reset.css');
		wp_enqueue_style('font', get_template_directory_uri() . '/css/font.css');
		wp_enqueue_style('colorbox', get_template_directory_uri() . '/css/colorbox.css');
		wp_enqueue_style('animate', get_template_directory_uri() . '/css/animate.css');

        wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery.js');
		wp_enqueue_script('jquery.animateNumber.min', get_template_directory_uri() . '/js/jquery.animateNumber.min.js');
		wp_enqueue_script('jquery.colorbox-min', get_template_directory_uri() . '/js/jquery.colorbox-min.js');
		wp_enqueue_script('jquery.enllax.min', get_template_directory_uri() . '/js/jquery.enllax.min.js');
		wp_enqueue_script('jquery.flexslider-min', get_template_directory_uri() . '/js/jquery.flexslider-min.js');
		wp_enqueue_script('masonry.pkgd.min', get_template_directory_uri() . '/js/masonry.pkgd.min.js');
        wp_enqueue_script('script', get_template_directory_uri() . '/js/script.js');
        wp_enqueue_script('investors', get_template_directory_uri() . '/js/lk_investors.js');
       // wp_enqueue_script('stickUp.min', get_template_directory_uri() . '/js/stickUp.min.js');
        wp_enqueue_script('viewportchecker', get_template_directory_uri() . '/js/viewportchecker.js');




}
	add_action('wp_enqueue_scripts', 'load_style_script');



//Post thumbnails
    add_theme_support ('post-thumbnails'); /*supporting of miniatures*/
	add_image_size( 'spec_thumb', 300, 300, true );
    add_theme_support( 'post-formats', array( 'gallery', 'quote','video', 'aside', 'image', 'link' ) );
	//Menu
if(function_exists('register_nav_menus')){
	register_nav_menus(
		array(
			'header_menu' => 'Top menu',
            'menu' => 'Main menu',
			'footer_menu' => 'Footer menu'
		)
	);
}
//Sidebar-WIDGETS
register_sidebar(array(
'name' => 'Widgets',
'id' => 'sidebar-widget',
'description' => 'Here is the place for sidebars widgets',
//'before_widget' => '',
//'after_widget' => '',
'before_title' => '<h3>',
'after_title' => '</h3>'
));
// for cut the long titles of posts
	function trim_title_chars($count, $after) {
  $title = get_the_title();
  if (mb_strlen($title) > $count) $title = mb_substr($title,0,$count);
  else $after = '';
  echo $title . $after;
}

// for cut the long texts of posts
function do_excerpt ($string, $word_limit){
	 $words = explode(' ', $string, ($word_limit + 1));
	 if (count($words) > $word_limit)
		 array_pop($words);
	 echo implode(' ', $words).' ...';
 }
// Breadcrumb
function the_breadcrumb() {
	if (!is_home()) {
		echo '<li><a href="';
		echo get_option('home').'">';
		echo 'Home';
		echo "</a> <span class='divider'>/</span></li> ";
		if (is_category() || is_single()) {
			echo "<li>";
			single_cat_title();
			echo "</li>";
			if (is_single()) {
			the_category(', ');
				echo " <span class='divider'>/</span><li> ";
				the_title();
				echo "</li>";
			}
		} elseif (is_page()) {
			echo "<li>";
			echo the_title();
			echo "</li>";
		}
		  elseif (is_tag()) {
			echo 'Posts with tag "'; 
			single_tag_title();
			echo '"'; }
		elseif (is_day()) {echo "Archive for"; the_time('  jS F Y');}
		elseif (is_month()) {echo "Archive "; the_time(' F  Y');}
		elseif (is_year()) {echo "Archive for "; the_time(' Y');}
		elseif (is_author()) {echo "Archive of author";}
		elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "Archive of blog";}
		elseif (is_search()) {echo "Results of search";}
			elseif (is_404()) {	echo '404 - Not found';}
	}
}
// for displaying the pictures of posts in slider
function catch_image()
{
    global $post, $posts;
    $first_img = '';
    ob_start();
    ob_end_clean();
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
    $first_img = $matches [1] [0];
    if(empty($first_img)) {
        $first_img = "wp-content/themes/hairsalon/images/slide1.png"; // if there are non photo in post we are using default photo
    }
    return $first_img;
}

add_action('init', 'topslider');
function topslider(){
	register_post_type('topslider', array(
		'public' => true,
        'supports' => array(
            'title',
            'editor',
            'author',
            'excerpt',
            'thumbnail'),
		'labels' => array(
			'name' => 'Top slider',
			'all_items' => 'All slides',
			'add_new' => 'Add new',
			'add_new_item' => 'Add new slide',
			'menu_position' => 5,
			'menu_icon' => ''
		)
	));
}
add_action ('init', 'videoreview');
function videoreview(){
    register_post_type('videoreview', array(
        'public' => true,
        'supports' => array(
            'title',
            'editor',
            'post-formats'),
        'labels' => array(
            'name' => 'Видео новости',
            'all_items' => 'Все видео',
            'add_new' => 'Добавить новое',
            'add_new_item' => 'Добавить видео',
            'menu_position' => 6,
            'menu_icon' => ''
        )
    ));
}
add_action ('init', 'right_block_articles');
function right_block_articles(){
	register_post_type('right_block_articles', array(
			'public' => true,
			'supports' => array(
				'title',
				'editor',
				'excerpt',
				'thumbnail'),
			'labels' => array(
				'name' => 'Аналитика новости',
				'all_items' => 'Все новости',
				'add_new' => 'Добавить новость',
				'add_new_item' => 'Новая новость',
				'show_ui' => true,
				'show_in_menu' => true,
				'menu_position' => 5,
				'menu_icon' => 'dashicons-format-aside'
			)

		)

	);
}

add_action ('init', 'personal_courses');
function personal_courses(){
	register_post_type('personal_courses', array(
			'public' => true,
			'supports' => array(
				'title',
				'excerpt',
				'editor',
				'thumbnail'),
				'labels' => array(
				'name' => 'Индивидуальные курсы',
				'all_items' => 'Все курсы',
				'add_new' => 'Добавить новый',
				'add_new_item' => 'Добавить курс',
				'menu_position' => 5,
				'menu_icon' => 'dashicons-book'
			)
		)
	);
}
add_action ('init', 'group_courses');
function group_courses(){
	register_post_type('group_courses', array(
			'public' => true,
			'supports' => array(
				'title',
				'editor',
				'excerpt',
				'thumbnail'),
			'labels' => array(
				'name' => 'Груповые курсы',
				'all_items' => 'Все курсы',
				'add_new' => 'Добавить новый',
				'add_new_item' => 'Добавить курс',
				'menu_position' => 5,
				'menu_icon' => 'dashicons-book-alt'
			)
		)
	);
}
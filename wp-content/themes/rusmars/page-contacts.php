<?php

/*
Template Name: Контакты
*/

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<title>Rusmars/Контакты</title>
	<link rel="icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" type="image/x-icon">

	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/animate.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/reset.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/font.css">
	
	
	<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/viewportchecker.js"></script>
	
	<script src="<?php bloginfo('template_url'); ?>/js/jquery.enllax.min.js"></script>

	<script src="<?php bloginfo('template_url'); ?>/js/jquery.animateNumber.min.js"></script>

	<!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

	<!-- Add fancyBox -->
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

	<!-- Optionally add helpers - button, thumbnail and/or media -->
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>



	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/colorbox.css">
	<script src="<?php bloginfo('template_url'); ?>/js/jquery.colorbox-min.js"></script>


	<script src="<?php bloginfo('template_url'); ?>/js/masonry.pkgd.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/stickUp.min.js"></script>
	
	<script src="<?php bloginfo('template_url'); ?>/js/jquery.flexslider-min.js"></script>

	<script src="<?php bloginfo('template_url'); ?>/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/dist/sweetalert.css">

	<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
	<script type="text/javascript">
        // Как только будет загружен API и готов DOM, выполняем инициализацию
        ymaps.ready(init);
 
        function init () {
            // Создание экземпляра карты и его привязка к контейнеру с
            // заданным id ("map")
            var myMap = new ymaps.Map('map', {
                    // При инициализации карты, обязательно нужно указать
                    // ее центр и коэффициент масштабирования
                    center: [55.7716,37.6826], // Нижний Новгород
                    zoom: 15
                });
 
				// Создание метки 
				var myPlacemark = new ymaps.Placemark(
				// Координаты метки
				[55.7716,37.6826] , {
                    // Свойства
                    // Текст метки
                    hintContent: 'Россия, г. Москва, ул. Ладожская, д. 7 (м. Бауманская).'
                }, {
                    iconImageHref: 'images/map.png', // картинка иконки
                    iconImageSize: [117, 102], // размеры картинки
                    iconImageOffset: [-32, -110] // смещение картинки
                    });     
 
 
				// Добавление метки на карту
				myMap.geoObjects.add(myPlacemark);
 
 
        }
    </script>
</head>
<body class="contacnt-page">
<div style="display: none;">
	<div id="form2">
		<div class="title">Заказать обратный звонок</div>
		<form class="ajaxForm" action="send.php" method="post">
			<input type="text" name="name" placeholder="Ваше имя" />
			<input type="text" name="phone" placeholder="Телефон" />
			<button type="submit">Отправить</button>
			<input type="hidden" name="btn" value="Оставьте заявку с формы: Позвоните мне!" />
		</form>
	</div>
</div>

	<div id="wrap">
		<div id="header">
			<div class="h-top">
				<div class="block-wrap">
				<div class="mob-btn1">Вход</div>
				<div class="mob-btn2"><a href="#">Регистрация</a></div>
					<form action="" id="form1">
						<div class="close" style="display: none;"></div>
						<div class="form-title">вход в личный <br> кабинет</div>
						<input type="email" placeholder="E-mail">
						<input type="password" placeholder="Пароль">
						<input type="submit" value="Войти">
					</form>

					<div class="help">
						<a href="">Справка</a>
					</div>

					<form action="" id="search">
						<input type="text">
						<input type="submit" value="Поиск">
					</form>
				</div>
			</div>

			<div class="h-bottom">
				<div class="block-wrap">
					<div id="logo">
						<a href="#"><img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt=""></a>
					</div>
					<div class="text">
						академия <br>
						профессионального <br>
						трейдинга
					</div>
					<div class="block1">
						<a href="personal--courses.php">Индивидуальные курсы</a>
						<a href="group--courses.php">Групповые курсы</a>
					</div>
					<div class="contacts">
						<div class="telephone">
							8 800 345 55 88
						</div>
						<a href="#form2" class="btn btn1">Обратный звонок</a>
					</div>
				</div>
			</div>
		</div> <!-- end-header -->	
		<div id="menu">
			<div class="block-wrap">
				<div class="menu-open">
					<span></span>
					<span></span>
					<span></span>
				</div>
				<ul class="menu-link">
					<div class="close" style="display: none;"></div>
					<span class="menu-title">меню</span>
					<li><a href="index.php">главная</a></li>
					<li><a href="analytics.php">аналитика</a></li>
					<li><a href="traders-club.php">Клуб трейдеров</a></li>
					<li><a href="investors.php">инвесторам</a></li>
					<li><a href="">экономический календарь</a></li>
					<li><a href="about-us.php">о нас</a></li>
					<li><a href="training.php">Обучение</a></li>	
					<li><a href="contacts.php" class="active">контакты</a></li>
				</ul>
			</div>
		</div> <!-- end menu -->

		<div class="h-bottom mob" style="display: none;">
			<div class="block-wrap">
				<div class="contacts">
					<div class="telephone">
						8 800 345 55 88
					</div>
					<a href="#form2" class="btn btn1">Обратный звонок</a>
				</div>
				<div class="block1">
					<a href="personal--courses.php">Индивидуальные курсы</a>
					<a href="group--courses.php">Групповые курсы</a>
				</div>
			</div>
		</div>

		<div id="content">
			<div class="block-wrap">
				<h1 class="page-title">Контакты</h1>
				<div id="map" style="width:719px; height:339px"></div>
				<div class="contacts">
					<div class="row field-address">
						<div class="label">
							Адрес:
						</div>
						<div class="body">
							Россия, г. Москва, ул. Ладожская, д. 7 (м. Бауманская).
						</div>
					</div>
					<div class="row field-telephone">
						<div class="label">
							Телефон:
						</div>
						<div class="body">
							8 800 345 55 88
						</div>
					</div>
					<div class="row field-telephone">
						<div class="label">
							Эл. почта
						</div>
						<div class="body">
							<a href="mailto:info@rusmars.ru">info@rusmars.ru</a>
						</div>
					</div>
				</div>
			</div>
		</div> <!-- end content -->
		<div class="triptych">
			<div class="block-wrap">
				
			</div>
		</div> <!-- end triptych -->

<?php get_footer(); ?>
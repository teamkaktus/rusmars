﻿<?php session_start(); //Запускаем сессии
require_once("scripts/auth.php");

$auth = new AuthClass();

if (isset($_POST["auth"])) {
    if (isset($_POST["login"]) && isset($_POST["password"])) { //Если логин и пароль были отправлены
        if (!$auth->auth($_POST["login"], $_POST["password"])) { //Если логин и пароль введен не правильно
            $notification = "Логин и пароль введен не правильно!";
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en"<?php language_attributes(); ?>>
<head>
    <meta meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?> charset="<?php bloginfo('charset'); ?>
    ">
    <meta name="viewport" content="width=device-width">
    <title><?php wp_title('&laquo;', true, 'right'); ?><?php bloginfo('name'); ?></title>
    <link rel="icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" type="image/x-icon">

    <script src="<?php bloginfo('template_url'); ?>/js/jquery.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/viewportchecker.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.enllax.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.animateNumber.min.js"></script>
    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript"
            src="<?php bloginfo('template_url'); ?>/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <!-- Add fancyBox -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/fancybox/source/jquery.fancybox.css" type="text/css"
          media="screen"/>
    <script type="text/javascript"
            src="<?php bloginfo('template_url'); ?>/fancybox/source/jquery.fancybox.pack.js"></script>
    <!-- Optionally add helpers - button, thumbnail and/or media -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/fancybox/source/helpers/jquery.fancybox-buttons.css"
          type="text/css" media="screen"/>
    <script type="text/javascript"
            src="<?php bloginfo('template_url'); ?>/fancybox/source/helpers/jquery.fancybox-buttons.js"></script>
    <script type="text/javascript"
            src="<?php bloginfo('template_url'); ?>/fancybox/source/helpers/jquery.fancybox-media.js"></script>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/fancybox/source/helpers/jquery.fancybox-thumbs.css"
          type="text/css" media="screen"/>
    <script type="text/javascript"
            src="<?php bloginfo('template_url'); ?>/fancybox/source/helpers/jquery.fancybox-thumbs.js"></script>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/colorbox.css">
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.colorbox-min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/masonry.pkgd.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.flexslider-min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/dist/sweetalert.css">
    <?php wp_head(); ?>

    <script>
        $(document).ready(function () {
            $(".inlineblock").find(function () {
                $(this).css("width", "100%");
            })
        });
    </script>
</head>
<body class="front">
<div style="display: none;">
    <div id="form2">
        <div class="title">Заказать обратный звонок</div>
        <form class="ajaxForm" action="send.php" method="post">
            <input type="text" name="name" placeholder="Ваше имя"/>
            <input type="text" name="phone" placeholder="Телефон"/>
            <button type="submit">Отправить</button>
            <input type="hidden" name="btn" value="Оставьте заявку с формы: Позвоните мне!"/>
        </form>
    </div>
</div>

<div id="wrap">
    <div id="header">
        <div class="h-top">
            <div class="block-wrap">
                <?php
                if ($_SESSION["is_auth"]) {
                    ?>

                    <a class="auth_in" href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/lk_investor/"><?php echo $_SESSION["login"] ?></a>
                    <?php
                } else {
                    ?>
                    <form id="form1" name="form1" action="<?php echo $_SERVER['REQUEST_URI'] ; ?>" method="post">
                        <div class="close" style="display: none;"></div>
                        <div class="form-title">вход в личный <br> кабинет</div>
                        <input id="email" type="email" placeholder="E-mail" name="login">
                        <input id="password" type="password" placeholder="Пароль" name="password">
                        <input type="submit" name="auth" value="Войти">
                        <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/registration/">Регистрация</a>
                    </form>

                    <?php
                }
                ?>


                <form action="" id="search">
                    <input type="text" name="s">
                    <input type="submit" value="Поиск">
                </form>
            </div>
        </div>

        <div class="h-bottom">
            <div class="block-wrap">
                <div id="logo">
                    <a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/logo.png"
                                                              alt=""></a>
                </div>
                <div class="text">
                    академия <br>
                    профессионального <br>
                    трейдинга
                </div>
                <div class="block1">
                    <?php wp_nav_menu(array('menu' => 'courses',
                        'before' => '')); ?>
                    <!--<a href="personal--courses.php">Индивидуальные курсы</a>
                    <a href="group--courses.php">Групповые курсы</a>-->
                </div>
                <div class="contacts">
                    <div class="telephone">
                        8 800 345 55 88
                    </div>
                    <a href="#form2" class="btn btn1">Обратный звонок</a>
                </div>
            </div>
        </div>
    </div> <!-- end-header -->
    <div id="menu">
        <div class="block-wrap">
            <div class="menu-open">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <ul class="menu-link">
                <div class="close" style="display: none;"></div>
                <span class="menu-title">меню</span>
                <li><a href="<?php bloginfo('url'); ?>/" class="active">главная</a></li>
                <?php
                $page_ID = get_the_ID();

                if ($page_ID == 34 || $page_ID == 36 || $page_ID == 38){
                    wp_nav_menu(array('menu' => 'lk',
                        'after' => ''));
                }else{
                    wp_nav_menu(array('menu' => 'main',
                        'after' => ''));
                }
                 ?>
                <!--<li><a href="analytics.php">аналитика</a></li>
                <li><a href="traders-club.php">Клуб трейдеров</a></li>
                <li><a href="investors.php">инвесторам</a></li>
                <li><a href="">экономический календарь</a></li>
                <li><a href="about-us.php">о нас</a></li>
                <li><a href="training.php">Обучение</a></li>
                <li><a href="contacts.php">контакты</a></li>-->
            </ul>
        </div>
    </div> <!-- end menu -->

    <div class="h-bottom mob" style="display: none;">
        <div class="block-wrap">
            <div class="contacts">
                <div class="telephone">
                    8 800 345 55 88
                </div>
                <a href="#form2" class="btn btn1">Обратный звонок</a>
            </div>
            <div class="block1">
                <?php wp_nav_menu(array('theme_location' => 'header_menu',
                    'before' => '')); ?>
                <!--<a href="personal--courses.php">Индивидуальные курсы</a>
                <a href="group--courses.php">Групповые курсы</a>-->
            </div>
        </div>
    </div>
    <div class="block-wrap">
        <h1 class="page-title"><?php the_title(); ?></h1>
    </div>
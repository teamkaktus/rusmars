<?php
/*
Template Name: Категории
*/
get_header(); ?>
    <div id="content">
        <div class="block-wrap">
            <h1><?php single_cat_title('Cathegory: '); ?></h1>
            <div class="field-content" style="float: left; width: 720px;">
                <div class="anal"> <!-- ;)) -->
                    <div class="row row-1">
                        <div class="field-title">
                            Видеообзор «Экспертное мнение»
                        </div>
                        <div class="field-video">
                            <div id="video">
                                <div id="a5yaBSxk1gk" class="youtube various fancybox.iframe" style="width: 304px; height: 360px;" href="https://www.youtube.com/embed/a5yaBSxk1gk?autoplay=1">
                                    <div class="play"></div>
                                </div>
                            </div>
                        </div>
                        <div class="field-body">
                            По информации деловых изданий, китайская компания CNPC заинтересовалась приватизацией «Роснефти». Возможная доля, которую может приобрести CNPC, будет обсуждаться на уровне российского и китайского лидеров. При этом китайскую компанию не интересует
                            приватизация «Башнефти», которая также планируется в текущем году.
                        </div>
                    </div>
                    
                <div class="entrance">
                    <div class="left">Для более точной информации вступайте клуб трейдеров!</div>
                    <div class="right"><a href="#">Вступить в клуб трейдеров</a></div>
                </div>
            </div>
        </div>
            <div class="sidebar-right">
                <div class="an-news">
                    <?php $right_block_articles = new WP_Query(array('post_type' => 'right_block_articles' )); ?>
                    <?php if($right_block_articles->have_posts()): ?><?php while($right_block_articles->have_posts()): $right_block_articles->the_post();?>
                    <div class="row">
                        <div class="block1">
                            <div class="field-img"><?php the_post_thumbnail(''); ?></div>
                            <div class="field-date"><?php the_time('d.F.Y.H.i.s'); ?>(МСК)</div>
                        </div>
                        <div class="field-title"><a href="<?php the_permalink (); ?>"><?php the_title(); ?></a></div>
                        <div class="field-body"><?php the_excerpt();?></div>
                    </div>
                </div>
                <?php endwhile; ?><?php else: ?>
                    <p>Here is supposed to be a a right block article</p>
                <?php endif; ?>
            </div> <!-- end right-sidebar -->
    </div> <!-- end content -->
<?php get_footer(); ?>
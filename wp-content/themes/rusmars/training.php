<?php

/*
Template Name: Обучение
*/
get_header();

?>

    <div id="content">
        <div class="block-wrap">
            <h1 class="page-title"><?= the_title(); ?></h1>
            <?php if (have_posts()) : while (have_posts()) : the_post();
            endwhile;
            else:
                _e('Страница пустая');
            endif; ?>
            <div class="training-block-1 banner">
                <?= the_post_thumbnail(); ?>
            </div>
            <div class="training-block-2">
                <div class="text">
                    <?= the_content(); ?>
                </div>
            </div>
        </div>
    </div> <!-- end content -->
    <div class="triptych">
        <div class="block-wrap">
            <h2>Список и цена курсов</h2>
            <div class="courses">
                <span class="course-caption">Индивидуальные курсы</span>
                <?php
                $personal_courses = new WP_Query(array('post_type' => 'personal_courses'));
                if ($personal_courses->have_posts()): ?><?php while ($personal_courses->have_posts()): $personal_courses->the_post();
                    $format_in = 'Ymd'; // the format your value is saved in (set in the field options)
                    $format_out = 'd-m-Y'; // the format you want to end up with

                    $date = DateTime::createFromFormat($format_in, get_field('_date_i_course'));
                    $price = get_field_object('_price_i_course');
                    ?>
                    <div class="row row-1">
                        <div class="bl-1">
                            <div class="field-date"><?= $date->format($format_out); ?></div>
                            <div class="field-title"><?= the_title(); ?></div>
                        </div>
                        <div class="bl-2">
                            <div class="field-body">
                                <?php the_excerpt(); ?>
                            </div>
                        </div>
                        <div class="bl-3">
                            <div class="field-price">
                                <div class="p-1">стоимость:</div>
                                <div class="p-2"><?= trim($price['value']) . ' ' . $price['append']; ?></div>
                            </div>
                            <div class="buy">
                                <a href="#">Оплатить</a>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?><?php else: ?>
                    <p>Сейчас нет добавленных курсов</p>
                <?php endif; ?>

            </div> <!-- end courses -->
            <div class="courses">
                <span class="course-caption">Групповые курсы</span>
                <?php
                $personal_courses = new WP_Query(array('post_type' => 'group_courses'));
                if ($personal_courses->have_posts()): ?><?php while ($personal_courses->have_posts()): $personal_courses->the_post();
                    $format_in = 'Ymd'; // the format your value is saved in (set in the field options)
                    $format_out = 'd-m-Y'; // the format you want to end up with

                    $date = DateTime::createFromFormat($format_in, get_field('_date_g_cource'));
                    $price = get_field_object('_price_g_cource');
                    ?>
                    <div class="row row-1">
                        <div class="bl-1">
                            <div class="field-date"><?= $date->format($format_out); ?></div>
                            <div class="field-title"><?= the_title(); ?></div>
                        </div>
                        <div class="bl-2">
                            <div class="field-body">
                                <?php the_excerpt(); ?>
                            </div>
                        </div>
                        <div class="bl-3">
                            <div class="field-price">
                                <div class="p-1">стоимость:</div>
                                <div class="p-2"><?= trim($price['value']) . ' ' . $price['append']; ?></div>
                            </div>
                            <div class="buy">
                                <a href="#">Оплатить</a>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?><?php else: ?>
                    <p>Сейчас нет добавленных курсов</p>
                <?php endif; ?>

            </div> <!-- end courses -->
        </div>
    </div> <!-- end triptych -->

<?php get_footer(); ?>
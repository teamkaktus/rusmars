<?php
/*
  Plugin Name: Custom Registration
  Plugin URI:
  Description:
  Version: 1.0
  Author: LF
  Author URI: http://example.com
 */
define('SHORTINIT', true);
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
global $wpdb;

function custom_email_exists($email)
{
    global $wpdb;
    $query = "SELECT id FROM users WHERE email = '" . $email . "'";
    //var_dump($query);
    return $wpdb->query($query);
}

function custom_phone_exists($phone)
{
    global $wpdb;
    $query = "SELECT id FROM users WHERE phone = '" . $phone . "'";
    //var_dump($query);
    return $wpdb->query($query);
}

function generate_password($number)
{
    $arr = array('a', 'b', 'c', 'd', 'e', 'f',
        'g', 'h', 'i', 'j', 'k', 'l',
        'm', 'n', 'o', 'p', 'r', 's',
        't', 'u', 'v', 'x', 'y', 'z',
        'A', 'B', 'C', 'D', 'E', 'F',
        'G', 'H', 'I', 'J', 'K', 'L',
        'M', 'N', 'O', 'P', 'R', 'S',
        'T', 'U', 'V', 'X', 'Y', 'Z',
        '1', '2', '3', '4', '5', '6',
        '7', '8', '9', '0');

    /*
        ,'.',',',
        '(',')','[',']','!','?',
        '&','^','%','@','*','$',
        '<','>','/','|','+','-',
        '{','}','`','~'
    */
    // Генерируем пароль
    $pass = "";
    for ($i = 0; $i < $number; $i++) {
        // Вычисляем случайный индекс массива
        $index = rand(0, count($arr) - 1);
        $pass .= $arr[$index];
    }
    return $pass;
}

function custom_registration_function()
{
    //var_dump($_POST);
    if (isset($_POST['u_submit'])) {
        registration_validation(
            $_POST['u_name'],
            $_POST['u_surname'],
            $_POST['u_day'],
            $_POST['u_month'],
            $_POST['u_year'],
            $_POST['u_citizenship'],
            $_POST['u_city'],
            $_POST['u_phone'],
            $_POST['u_email'],
            $_POST['u_agree']
            //$_POST['password']
        );

        // sanitize user form input
        global $name, $surname, $day, $month, $year, $citizenship, $city, $phone, $email, $agree;
        $name        = sanitize_text_field($_POST['u_name']);
        $surname     = sanitize_text_field($_POST['u_surname']);
        $day         = sanitize_text_field($_POST['u_day']);
        $month       = sanitize_text_field($_POST['u_month']);
        $year        = sanitize_text_field($_POST['u_year']);
        $citizenship = sanitize_text_field($_POST['u_citizenship']);
        $city        = sanitize_text_field($_POST['u_city']);
        $email       = sanitize_email($_POST['u_email']);
        $phone       = sanitize_text_field($_POST['u_phone']);
        if($_POST['u_agree'] == "on"){
            $agree = "on";
        } else{
            $agree = "off";
        }
        // call @function complete_registration to create the user
        // only when no WP_error is found
        complete_registration(
            $name,
            $surname,
            $day,
            $month,
            $year,
            $citizenship,
            $city,
            $phone,
            $email,
            $agree
        );
    }

    registration_form(
        $name,
        $surname,
        $day,
        $month,
        $year,
        $citizenship,
        $city,
        $phone,
        $email,
        $agree
    );
}

function registration_form($name, $surname, $day, $month, $year, $citizenship, $city, $phone, $email, $agree)
{


    $regform = "<form action='" .   $_SERVER['REQUEST_URI'] . "' method=\"post\">
   <div class=\"left\">
                    <div class=\"field-form f-name\">
                        <div class=\"label\">Ваше имя<span class=\"green\">*</span></div>
                        <input type=\"text\" name=\"u_name\">
                    </div>
                    <div class=\"field-form l-name\">
                        <div class=\"label\">Ваша фамилия</div>
                        <input type=\"text\" name=\"u_surname\">
                    </div>
                    <div class=\"field-form birth\">
                        <div class=\"label\">Дата рождения</div>
                        <select class=\"day\" name=\"u_day\">
                            <option value='-1'>День</option>";
    for($i = 1; $i <=31; $i++){
        $regform .= "<option value='".$i."'>".$i."</option>";
    }

    $regform .= "</select>
                        <select class=\"month\" name=\"u_month\">
                            <option  value='-1'>Месяц</option>
                            <option value=\"01\">Январь</option>
                            <option value=\"02\">Февраль</option>
                            <option value=\"03\">Март</option>
                            <option value=\"04\">Апрель</option>
                            <option value=\"05\">Май</option>
                            <option value=\"06\">Июнь</option>
                            <option value=\"07\">Июль</option>
                            <option value=\"08\">Август</option>
                            <option value=\"09\">Сентябрь</option>
                            <option value=\"10\">Октябрь</option>
                            <option value=\"11\">Ноябрь</option>
                            <option value=\"12\">Декабрь</option>
                        </select>
                        <select class=\"year\" name=\"u_year\">
                            <option  value='-1'>Год</option>";

    for($i = 1940; $i <=(2016-18); $i++){
        $regform .= "<option value='$i'>$i</option>";
    }

    $regform .="</select>
                    </div>
                    <div class=\"field-form citizenship\">
                        <div class=\"label\">Гражданство<span class=\"green\">*</span></div>
                        <input type=\"text\" name=\"u_citizenship\">
                    </div>
                </div> <!-- end left -->
                <div class=\"right\">
                    <div class=\"field-form city\">
                        <div class=\"label\">Город<span class=\"green\">*</span></div>
                        <input type=\"text\" name=\"u_city\">
                    </div>
                    <div class=\"field-form telephone\">
                        <div class=\"label\">Телефон</div>
                        <input type=\"text\" name=\"u_phone\">
                    </div>
                    <div class=\"field-form email\">
                        <div class=\"label\">E-mail<span class=\"green\">*</span></div>
                        <input type=\"email\" name=\"u_email\">
                    </div>
                    <div class=\"checkbox1\">
                        <input id=\"checkbox1\" type=\"checkbox\" name=\"u_agree\" checked>
                        <label for=\"checkbox1\">Я согласен на обработку персональных данных</label>
                    </div>
                    <div class=\"btn-sub\">
                        <input type=\"submit\" name=\"u_submit\" value=\"Зарегистрироваться\">
                    </div>
                    <div class=\"inf\">
                        <span class=\"green\">*</span> Поля ,отмеченные звездочкой, обязательны заполению
                    </div>
                </div>
                </form>";
    echo $regform;
}

function registration_validation($name, $surname, $day, $month, $year, $citizenship, $city, $phone, $email, $agree)
{
    global $reg_errors;
    $reg_errors = new WP_Error;

    if($agree != "on"){
        $reg_errors->add('field', 'Вы не согласились с обработкой персональных данных');
    }
    if (empty($name) || empty($citizenship) || empty($city) || empty($email)) {
        $reg_errors->add('field', 'Не все поля заполнены');
    }

    if(!checkdate($month, $day, $year)){
        $reg_errors->add('date', 'Дата не корректна');
    }

    if (!preg_match("/^[a-z,A-Z,0-9,а-яіїєґ,А-ЯІЇЄҐ,-,_]/", strtolower($surname))) {
        $reg_errors->add('surname_invalid', 'Фамилия не корректна');
    }
    if (!preg_match("/^[a-z,A-Z,0-9,а-яіїєґ,А-ЯІЇЄҐ,-,_]/", strtolower($name))) {
        $reg_errors->add('name_invalid', 'Имя не корректно');
    }

    if (!preg_match("/^[a-z,A-Z,0-9,а-яіїєґ,А-ЯІЇЄҐ,-,_]/", strtolower($citizenship))) {
        $reg_errors->add('middlename_invalid', 'Гражданство не корректно');
    }

    if (!preg_match("/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/", strtolower($phone))) {
        $reg_errors->add('phone_invalid', 'Телефон не корректен');
    }
    if (custom_phone_exists($phone)) {
        $reg_errors->add('phone', 'Телефон уже используется');
    }

    if (!is_email($email)) {
        $reg_errors->add('email_invalid', 'Почта не коректна');
    }

    if (custom_email_exists($email)) {
        $reg_errors->add('email', 'Почтовый ящик уже используется');
    }


    if (is_wp_error($reg_errors)) {

        foreach ($reg_errors->get_error_messages() as $error) {
            echo '<div>';
            echo '<strong>ERROR</strong>:';
            echo $error . '<br/>';

            echo '</div>';
        }
    }
}

function complete_registration()
{
    global $reg_errors, $wpdb, $name, $surname, $day, $month, $year, $citizenship, $city, $phone, $email, $agree;

    if (count($reg_errors->get_error_messages()) < 1) {
        /* $userdata = array(
             'name'	       => $name,
             'surname'     => $surname,
             'birthday'    => $birthday,
             'citizenship' => $citizenship,
             'city' 	   => $city,
             'phone' 	   => $phone,
             'email' 	   => $email,
             'password'    => $password,

         );*/
        //$user = wp_insert_user( $userdata );echo("УДАЧНЫЙ РЕГ");
        $regdate = "" . date('d.m.Y');

        $birthday = $day.".".$month.".".$year;

        $password = "" . generate_password(8);

        $password_hash = crypt($password);

        $query = "INSERT INTO users
        VALUES ('',
         '" . $name . "',
         '" . $surname . "',
         '" . $birthday . "',
         '" . $citizenship . "',
         '" . $city . "',
         '" . $phone . "',
         '" . $email . "',
         '" . $password_hash . "',
         '" . $regdate . "'
         )";
       /* var_dump($query);
        exit;*/
        if ($wpdb->query($query)) {
            echo 'Регистрация завершена';


            /* получатели */
            $to = "Agent <" . $email . ">";

            /* тема/subject */
            $subject = "Успешная регистрация";

            /* сообщение */
            $message = '
            <html>
            <head>
             <title>Регистрация прошла успешно</title>
            </head>
            <body>
            <p>Данные Регистрации:</p>
            <p>Логин</p>
            <p>' . $email . '</p>
            <p>Пароль</p>
            <p>' . $password . '</p>
            </body>
            </html>
            ';

            /* Для отправки HTML-почты вы можете установить шапку Content-type. */
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=utf-8\r\n";

            /* дополнительные шапки */
            /*$headers .= "From: Birthday Reminder <birthday@example.com>\r\n";
            $headers .= "Cc: birthdayarchive@example.com\r\n";
            $headers .= "Bcc: birthdaycheck@example.com\r\n";*/

            /* и теперь отправим из */
            mail($to, $subject, $message, $headers);

        } else {
            echo 'Ошибка регистрация';
        }

    }
}

// Register a new shortcode: [cr_custom_registration]
add_shortcode('cr_custom_registration', 'custom_registration_shortcode');

// The callback function that will replace [book]
function custom_registration_shortcode()
{
    ob_start();
    custom_registration_function();
    return ob_get_clean();
}
